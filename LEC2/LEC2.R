options(warn=1)
library(ggplot2)

# Pregunta 1.1
# ------------
# 
SacarUnaCarta <- function(n.times) {
  # Retorna una tabla con la frecuencia relativa de las cartas
  # del mazo de naipe espanol elegidas al azar.
  pintas <- c('Oro', 'Basto', 'Espada', 'Copa')
  # Generar combinaciones como "Oro 1", "Basto 8", "Espada 3" y
  # "Copa 10".
  mazo <- paste(rep(pintas, 4), rep(c(1:10), each=4))
  return(table(sample(mazo, n.times, replace=TRUE)) / n.times)
}

plot(SacarUnaCarta(1000000), xlab = 'Suma',
     ylab = "Frecuencia relativa",
     main = "Sacar una carta al azar 1.000.000 de veces",
     las=2,
     cex.axis=0.6)

# Pregunta 1.2
# ------------
# 
SacarDosCartaSinReemplazo <- function(n.times) {
  pintas <- c('Oro', 'Basto', 'Espada', 'Copa')
  mazo <- paste(rep(pintas, 4), rep(c(1:10), each=4))
  dos.cartas <- replicate(n.times, sample(mazo, 2, replace=FALSE))
  primeras.filas.10 <- matrix(integer(0), nrow=2, ncol=0)
  for (n.col in 1:ncol(dos.cartas)) {
    if (grepl('10', dos.cartas[1, n.col]) & grepl('1(?!0)', dos.cartas[2, n.col], perl=TRUE)) {
      # El experimento solo continua si la primera carta extraida de cualquier
      # pinta y sin reemplazo es un 1, y es exitoso cuando la segunda, tambien
      # de cualquier pinta, es un 1.
      primeras.filas.10 <- cbind(primeras.filas.10, dos.cartas[, n.col])
    }
  }
  return(table(primeras.filas.10[2, ]) / (length(primeras.filas.10) * 10))
  # return(table(primeras.filas.10) / n.times)
}

plot(SacarDosCartaSinReemplazo(100000),
     xlab = "Luego de haber robado una carta 10 de cualquier pinta",
     ylab = "Frecuencia relativa",
     main = "100.000 robos de cartas")

# Pregunta 1.3
# ------------
#
EntregarTresCartas <- function(n.times) {
  # Retorna una matriz en donde cada columna representa una mano de 3 cartas
  # diferentes numeradas del 1 al 10.
  #
  # Tomar 3 cartas numeradas del 1 al 10 sin devolucion al mazo
  # (sin reemplazo).
  return(replicate(n.times, sample(c(1:10), 3, replace=FALSE)))
}

CalcularFrecRelManos<- function(vec.manos) {
  # Returna una tabla con la frecuencia relativa de cada mano.
  #
  # Obtener frecuencia absoluta.
  suma.vec.manos <- colSums(vec.manos)
  # Retornar suma.vec.manos como tabla y con la frecuancia realativa.
  return(table(suma.vec.manos) / length(suma.vec.manos))
}

plot(CalcularFrecRelManos(EntregarTresCartas(100000)),
     xlab = 'Suma',
     ylab = "Frecuencia relativa",
     main = "100.000 manos de 3 naipes espa\u00f1oles")

#Pregunta 2.1
#Devuelve un vector con "n" numeros que representan
#que tan lejos estan del centro del blanco.
#Se hizo de tal forma para que no se puedan salir del blanco.
lanzarDardos <- function(n)
{
  d <- vector()
  i <- 1
  while (i < n+1)
  {
    posX <- runif(1, -9.5, 9.5)
    posY <- runif(1, -9.5, 9.5)
    
    distance <- sqrt( ((posX[1])^2) + ((posY[1])^2) )
    if ( distance <= 9.5 )
    {
      d[i] <- distance
      i <- i + 1
    }
  }
  return(d)
}
z<-lanzarDardos(1000)
hist(z, 
     breaks=c(0, 0.5, 1.5, 2.5, 3.5, 4.5, 9.5),
     freq = FALSE, 
     main = paste("Distribución de 1000 tiros"),
     xlab = "Distancia del centro", 
     ylab = "Frecuencia dividido 1000",
     axes = FALSE)
axis(1, at=seq(0, 0.5, by=0.5), labels=seq(0, 0.5, by=0.5))
axis(1, at=seq(0.5, 4.5, by=1), labels=seq(0.5, 4.5, by=1))
axis(1, at=seq(4.5, 9.5, by=1), labels=seq(4.5, 9.5, by=1))
axis(2, at=seq(0.00, 0.088, by=0.022), labels=seq(0.00, 0.088, by=0.022))

#Verificaba que la densidad que muestra el grafico
# correspondian a las probabilidades teoricas
# (Probabilidades teoricas en el doc)
extraerDardos <- function(dardos, a, b)
{
  i <- 1
  total <- 0
  while (i < length(dardos))
  {
    if( (a <= dardos[i]) & (dardos[i] <= b) )
    {
      total <- total + 1
    }
    i <- i + 1
  }
  return(total)
}
bullseye<-extraerDardos(z, 0, 0.5)
bullseye
premio9<-extraerDardos(z, 0.5, 1.5)
premio9
premio8<-extraerDardos(z, 1.5, 2.5)
premio8
premio7<-extraerDardos(z, 2.5, 3.5)
premio7
premio6<-extraerDardos(z, 3.5, 4.5)
premio6
noPremio<-extraerDardos(z, 4.5, 9.5)
noPremio

#Pregunta 2.2
#Esta funcion rellena con 6.5 cuando no acierta
lanzarDardosPreparados <- function(n)
{
  d <- vector()
  i <- 1
  ensartado <- 0
  while (i < n+1)
  {
    posX <- runif(1, -5.5, 5.5)
    posY <- runif(1, -5.5, 5.5)
    
    distance <- sqrt( ((posX[1])^2) + ((posY[1])^2) )
    if ( distance <= 5.5 )
    {
      ensartado <- runif(1, 0, 1)
      if ( ensartado <= 0.9 )
      {
        d[i] <- distance
      }
      else
      {
        d[i] <- (6.5)
      }
      i <- i + 1
    }
  }
  return(d)
}
z<-lanzarDardosPreparados(1000)

#La razon de porque incluyo las fallas,
#es porque el histograma siempre excluye los
#NA. No encontre forma de arreglarlo asique
#tuve que incluir las fallas con valor 6.5
hist(z, 
     breaks=c(0, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5),
     freq = FALSE, 
     main = paste("Distribuci\u00f3n de 1000 tiros"),
     xlab = "Distancia del centro y fallos", 
     ylab = "Frecuencia dividido 1000",
     axes = FALSE)
axis(1, at=seq(0, 0.5, by=0.5), labels=seq(0, 0.5, by=0.5))
axis(1, at=seq(0.5, 5.5, by=1), labels=seq(0.5, 5.5, by=1))
axis(2, at=seq(0.00, 0.3, by=0.06), labels=seq(0.00, 0.3, by=0.06))

#Esto cuenta los NA en "z", osea, cuantas veces fallo
fallas <- function(lanzamientos)
{
  i <- 1
  totalFallas <- 0
  while( i < length(lanzamientos))
  {
    if ( lanzamientos[i] == 6.5 )
    {
      totalFallas <- totalFallas + 1
    }
    i <- i + 1
  }
  return(totalFallas)
}
totalFallas <- fallas(z)
totalFallas
#Transformamos todo esto en un dataframe para graficar
lanzamientosTipo <- c("Aciertos", "Fallos")
lanzamientosFrecuencia <- c((1000-totalFallas), totalFallas)
df <- data.frame(lanzamientosTipo, lanzamientosFrecuencia)
df

p<-ggplot(data=df, aes(x=lanzamientosTipo, y=lanzamientosFrecuencia))
p<-p+geom_bar(stat="identity", width=0.5, position = "dodge")
p<-p+scale_y_continuous(breaks = seq(0, 1000, by = 100))
p<-p+labs(title = "Aciertos vs Fallos")
p<-p+labs(x = "Resultado del lanzamiento")
p<-p+labs(y = "Frecuencia")
p

#Pregunta 3.1
madres <- c(1, 2, 3, 4)

#Los bebes tipo "1" seran los enfermos

#Aqui estaba probando cosas
bebes <- sample(madres, replace=TRUE, size=2000)
bebes <- table(bebes)
bebes
bebes[1]
bebes[2]
bebes[3]
bebes[4]
as.data.frame(bebes)

#Esta funcion entrega un vector con 
# el numero de bebes enfermos en 
# cada simulacion
simularEnfermos <- function(simulaciones)
{
  bebesEnfermos <- vector()
  i <- 1
  while( i <= simulaciones )
  {
    bebes <- sample(madres, replace=TRUE, size=2000)
    bebes <- table(bebes)
    bebesEnfermos[i] <- bebes[1]
    i <- i + 1
  }
  return(bebesEnfermos)
}
#Simular 100 meses
dfBebes <- simularEnfermos(100)

#Construyo un dataframe para graficar
numeroSimulacion <- seq.int(from=1, to=100, by=1)
numeroSimulacion
dfBebes <- data.frame(dfBebes, numeroSimulacion)
dfBebes

#Grafico
p <- ggplot(dfBebes, aes(x=numeroSimulacion, y=dfBebes))
p <- p + geom_point(size=2, shape=23)
p <- p + scale_y_continuous(limits = c(250,750))
p <- p + labs(title = "Bebes enfermos por simulaci\u00f3n")
p <- p + labs(x = "Numero de simulaci\u00f3n")
p <- p + labs(y = "Bebes enfermos en simulaci\u00f3n")
p <- p + geom_hline(yintercept = 500)
p

#Pregunta 3.2
dfBebes <- simularEnfermos(500)
#Esto cuenta los cuando una simulacion
# obtuvo mas de 500 bebes enfermos
mesesSinAtencion <- function(simulaciones)
{
  i <- 1
  totalSinAtencion <- 0
  while( i < length(simulaciones))
  {
    if ( simulaciones[i] > 500 )
    {
      totalSinAtencion <- totalSinAtencion + 1
    }
    i <- i + 1
  }
  return(totalSinAtencion)
}
hospitalFalla <- mesesSinAtencion(dfBebes)
hospitalFalla

numeroSimulacion <- seq.int(from=1, to=500, by=1)
dfBebes <- data.frame(dfBebes, numeroSimulacion)
dfBebes

#Grafico
p <- ggplot(dfBebes, aes(x=numeroSimulacion, y=dfBebes))
p <- p + geom_point(size=2, shape=23)
p <- p + scale_y_continuous(limits = c(250,750))
p <- p + labs(title = "Bebes enfermos por simulaci\u00f3n")
p <- p + labs(x = "Numero de simulaci\u00f3n")
p <- p + labs(y = "Bebes enfermos en simulaci\u00f3n")
p <- p + geom_hline(yintercept = 500)
p


#Transformamos todo esto en un dataframe para graficar
atencionTipo <- c("Correcto", "Fallo")
atencionFrecuencia <- c((500-hospitalFalla), hospitalFalla)
df <- data.frame(atencionTipo, atencionFrecuencia)
df

p<-ggplot(data=df, aes(x=atencionTipo, y=atencionFrecuencia))
p<-p+geom_bar(stat="identity", width=0.5, position = "dodge")
p<-p+scale_y_continuous(breaks = seq(0, 300, by = 50))
p<-p+labs(title = "Atenciones correctas vs Falta de atencion")
p<-p+labs(x = "Resultado del simulaciones")
p<-p+labs(y = "Frecuencia")
p

#Pregunta 3.3

#Funcion especial para esta pregunta
# fuerza que en la primera semana hallan
# 150 enfermos
simularEnfermos <- function(simulaciones)
{
  bebesEnfermos <- vector()
  i <- 1
  while( i <= simulaciones )
  {
    bebes <- sample(madres, replace=TRUE, size=1500)
    enfermos <- c(rep.int(1,150),rep.int(2,350))
    bebes <- table(bebes)
    tmpBebes <- c(bebes,enfermos)
    tmpBebes <- table(tmpBebes)
    bebesEnfermos[i] <- tmpBebes[1]
    i <- i + 1
  }
  return(bebesEnfermos)
}
dfBebes <- simularEnfermos(500)
hospitalFalla <- mesesSinAtencion(dfBebes)
hospitalFalla

numeroSimulacion <- seq.int(from=1, to=500, by=1)
dfBebes <- data.frame(dfBebes, numeroSimulacion)
dfBebes

#Grafico
p <- ggplot(dfBebes, aes(x=numeroSimulacion, y=dfBebes))
p <- p + geom_point(size=2, shape=23)
p <- p + scale_y_continuous(limits = c(250,750))
p <- p + labs(title = "Bebes enfermos por simulaci\u00f3n")
p <- p + labs(x = "Numero de simulaci\u00f3n")
p <- p + labs(y = "Bebes enfermos en simulaci\u00f3n")
p <- p + geom_smooth(method = "lm", se = FALSE)
p

#Transformamos todo esto en un dataframe para graficar
atencionTipo <- c("Correcto", "Fallo")
atencionFrecuencia <- c((500-hospitalFalla), hospitalFalla)
df <- data.frame(atencionTipo, atencionFrecuencia)
df

p<-ggplot(data=df, aes(x=atencionTipo, y=atencionFrecuencia))
p<-p+geom_bar(stat="identity", width=0.5, position = "dodge")
p<-p+scale_y_continuous(breaks = seq(0, 300, by = 50))
p<-p+labs(title = "Atenciones correctas vs Falta de atencion")
p<-p+labs(x = "Resultado del simulaciones")
p<-p+labs(y = "Frecuencia")
p
